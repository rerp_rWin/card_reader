<?php
declare(strict_types=1);

require "./vendor/autoload.php";

$websocket = new \WebSocket\Client('ws://127.0.0.1:8081');

while(true) {
    try {
        $message = json_decode($websocket->receive(), true);
        var_dump($message); echo PHP_EOL;
    } catch (\WebSocket\TimeoutException $exception) {
        echo 'no new messages' . PHP_EOL;
    } catch (\WebSocket\ConnectionException $exception) {
        echo 'There was a problem with connection' . PHP_EOL;
        $websocket->disconnect();
    }
}
