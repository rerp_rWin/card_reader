<?php

namespace CardReader;

use Ratchet\ConnectionInterface;
use Ratchet\WebSocket\MessageComponentInterface;

class WebsocketCardConnection implements MessageComponentInterface
{
    private ?ConnectionInterface $connection = null;
    private LoopThroughCardState $loopThroughCardState;

    function onOpen(ConnectionInterface $conn)
    {
        $this->connection = $conn;
        $conn->send($this->loopThroughCardState->getCurrentState()->getJson());
    }

    function onClose(ConnectionInterface $conn)
    {
        $this->connection = null;
        $conn->close();
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->connection = null;
    }

    function onMessage(ConnectionInterface $from, $msg)
    {
        //??
    }

    function getSubProtocols()
    {
        // ???
    }

    public function getConnection(): ?ConnectionInterface
    {
        return $this->connection;
    }

    public function setLoopThroughCardState(LoopThroughCardState $loopThroughCardState): void
    {
        $this->loopThroughCardState = $loopThroughCardState;
    }
}
