<?php

namespace CardReader;

use CardReader\Dto\AbstractMessageDto;
use CardReader\Dto\EgkDataDto;
use CardReader\Dto\FatalMessageDto;
use CardReader\Dto\InitializingDto;
use CardReader\Dto\InvalidCardDto;
use CardReader\Dto\NoCardConnectedDto;
use CardReader\Dto\NoReaderConnectedDto;
use CardReader\Exception\RecoverableException;
use CardReader\Exception\StatusFalseException;

class LoopThroughCardState
{
    private AbstractMessageDto $currentState;
    private WebsocketCardConnection $cardConnection;

    public const POSSIBLE_ATRS_FOR_GERMAN_HEALTH_CARDS = [
        "3bd096ff81b1fe451f032e",
        "3bd096ff81b1fe451f072a",
        "3bd096ff81b1fe451fc7ea",
        "3bd097ff81b1fe451f072b"
    ];
    private PCSCWrapper $pcsc;
    private array|bool $status = false;

    public function __construct()
    {
        $this->pcsc = new PCSCWrapper();
        $this->currentState = new InitializingDto();
    }

    public function __invoke()
    {
        $pcsc = $this->pcsc;
        $newState = $this->currentState;

        try {
            if ($this->status) {
                if ($pcsc->getStatus()) {
                    return;
                }
                $this->status = false;
            }

            // get readers
            if (!$readers = $pcsc->listReaders()) {
                // no reader connected
                $newState = new NoReaderConnectedDto();
                throw new RecoverableException();
            }

            // connect to card
            if (!$pcsc->connect($readers[0])) {
                // no card connected
                $newState = new NoCardConnectedDto();
                throw new RecoverableException();
            }

            $personalDataXml = $this->getPersonalDataXml();
            $insuranceDataXml = $this->getInsuranceDataXml();

            if (!$personalDataXml || !$personalData = simplexml_load_string($personalDataXml)) {
                $newState = new InvalidCardDto();
                throw new RecoverableException();
            }
            if (!$insuranceDataXml || !$insuranceData = simplexml_load_string($insuranceDataXml)) {
                $newState = new InvalidCardDto();
                throw new RecoverableException();
            }

            // get information
            $newState = new EgkDataDto();
            $newState->setInsuranceData($insuranceData);
            $newState->setPersonalData($personalData);

            $this->status = $pcsc->getStatus();

        } catch (RecoverableException $e) {
            // do nothing - its fine
        } catch (StatusFalseException $e) {
            $newState = new NoCardConnectedDto();
            $this->status = false;
            $pcsc->resetConnection();
        } catch (\Throwable $e) {
            $newState = (new FatalMessageDto())->setException($e);
        } finally {
            if ($newState->getStatus() !== $this->currentState->getStatus()) {
                $this->cardConnection->getConnection()?->send(json_encode($newState));
            }
            $this->currentState = $newState;
        }
    }

    public function getCurrentState(): ?AbstractMessageDto
    {
        return $this->currentState;
    }

    public function setCardConnection(WebsocketCardConnection $cardConnection): void
    {
        $this->cardConnection = $cardConnection;
    }

    public function getPersonalDataXml(): string|false
    {
        $pcsc = $this->pcsc;

        $pcsc->transmit(implode('', \COMMANDS::SELECT_MF));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_HCA));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_FILE_PD));

        $command = \COMMANDS::READ_BINARY(0x00, 0x02);
        $response = $pcsc->transmit($command);

        $length = hexdec($response);
        $currentLength = 0;
        $chunks = [];

        $maxLength = 0xFD; // leave 2 bytes for status
        $currentPos = 0x02;

        $pcsc->transmit(implode('', \COMMANDS::SELECT_MF));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_HCA));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_FILE_PD));

        while ($currentLength < $length) {
            $bytesLeft = $length - $currentLength;
            $readLength = ($bytesLeft < $maxLength) ? $bytesLeft : $maxLength;
            $command = \COMMANDS::READ_BINARY($currentPos, $readLength);
            $chunk = $pcsc->readBytes($command);
            $currentLength += $readLength;
            $currentPos += $readLength;
            if (str_ends_with($chunk, '9000')) {
                // success and continue
                $chunks[] = substr($chunk, 0, -4);
            } else if (str_ends_with($chunk, '6282')) {
                // success and stop because it is empty
                $chunks[] = substr($chunk, 0, -4);
                break;
            } else if (strlen($chunk) === 4 && str_starts_with($chunk, '6B00')) {
                // parameter is too large
                break;
            } else {
                throw new \Exception('Unable to recongnize last part of zip file');
            }
        }

        $arr = array_merge($chunks);
        $compressedFile = join($arr);
        $compressedFileContents = pack('H*', $compressedFile);

        $personalDataXml = zlib_decode($compressedFileContents);
        return $personalDataXml;
    }

    private function getInsuranceDataXml(): string|false
    {
        $pcsc = $this->pcsc;

        $pcsc->transmit(implode('', \COMMANDS::SELECT_MF));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_HCA));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_FILE_VD));

        $command = \COMMANDS::READ_BINARY(0x00, 0x04);

        $chunk = $pcsc->transmit($command);

        $position = str_split($chunk, 4);

        $length = hexdec($position[1]) - hexdec($position[0]);
        $currentPos = hexdec($position[0]);
        $currentLength = 0;
        $chunks = [];

        $maxLength = 0xFD; // leave 2 bytes for status

        $pcsc->transmit(implode('', \COMMANDS::SELECT_MF));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_HCA));
        $pcsc->transmit(implode('', \COMMANDS::SELECT_FILE_VD));

        while ($currentLength < $length) {
            $bytesLeft = $length - $currentLength;
            $readLength = ($bytesLeft < $maxLength) ? $bytesLeft : $maxLength;
            $command = \COMMANDS::READ_BINARY($currentPos, $readLength);
            $chunk = $pcsc->readBytes($command);
            $currentLength += $readLength;
            $currentPos += $readLength;
            if (str_ends_with($chunk, '9000')) {
                // success and continue
                $chunks[] = substr($chunk, 0, -4);
            } else if (str_ends_with($chunk, '6282')) {
                // success and stop because it is empty
                $chunks[] = substr($chunk, 0, -4);
                break;
            } else if (strlen($chunk) === 4 && str_starts_with($chunk, '6B00')) {
                // parameter is too large
                break;
            } else {
                throw new \Exception('Unable to recongnize last part of zip file');
            }
        }

        $arr = array_merge($chunks);
        $compressedFile = join($arr);
        $compressedFileContents = pack('H*', $compressedFile);

        $insuranceDataXml = zlib_decode($compressedFileContents);
        return $insuranceDataXml;
    }
}
