<?php

namespace CardReader\Dto;

class FatalMessageDto extends AbstractMessageDto
{
    public const STATUS_MESSAGE = 'FATAL';
    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }

    function setException(\Throwable $e): self
    {
        $message = [
            'exception' => $e::class,
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'trace' => $e->getTraceAsString()
        ];

        return $this->setMessage(
            $message
        );
    }
}
