<?php

namespace CardReader\Dto;

abstract class AbstractMessageDto implements \JsonSerializable
{
    private ?array $message = null;

    public function jsonSerialize()
    {
        return [
            'status' => $this->getStatus(),
            'message' => $this->getMessage()
        ];
    }

    public function getJson(): string
    {
        return json_encode($this);
    }

    public function getMessage(): null|array|\JsonSerializable
    {
        return $this->message;
    }

    public function setMessage(null|array|\JsonSerializable $message): self
    {
        $this->message = $message;
        return $this;
    }

    abstract function getStatus(): string;
}
