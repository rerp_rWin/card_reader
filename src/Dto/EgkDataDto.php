<?php

namespace CardReader\Dto;

class EgkDataDto extends AbstractMessageDto
{

    public const STATUS_MESSAGE = 'Ekg Data found';

    private ?\SimpleXMLElement $personalData = null;
    private ?\SimpleXMLElement $insuranceData = null;

    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }

    /**
     * @param \SimpleXMLElement|null $personalData
     */
    public function setPersonalData(?\SimpleXMLElement $personalData): void
    {
        $this->personalData = $personalData;
    }

    /**
     * @param \SimpleXMLElement|null $insuranceData
     */
    public function setInsuranceData(?\SimpleXMLElement $insuranceData): void
    {
        $this->insuranceData = $insuranceData;
    }

    public function jsonSerialize()
    {
        return [
            'status' => $this->getStatus(),
            'message' => [
                'personalData' => $this->personalData,
                'insuranceData' => $this->insuranceData
            ]
        ];
    }
}
