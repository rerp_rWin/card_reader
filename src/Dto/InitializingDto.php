<?php

namespace CardReader\Dto;

class InitializingDto extends AbstractMessageDto
{

    public const STATUS_MESSAGE = 'Initializing Application';

    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }
}
