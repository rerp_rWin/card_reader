<?php

namespace CardReader\Dto;

class NoCardConnectedDto extends AbstractMessageDto
{

    public const STATUS_MESSAGE = "No Card connected";

    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }
}
