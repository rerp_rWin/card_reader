<?php

namespace CardReader\Dto;

class NoReaderConnectedDto extends AbstractMessageDto
{

    public const STATUS_MESSAGE = 'No Reader Connected';

    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }
}
