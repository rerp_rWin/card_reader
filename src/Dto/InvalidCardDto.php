<?php

namespace CardReader\Dto;

class InvalidCardDto extends AbstractMessageDto
{

    public const STATUS_MESSAGE = 'Invalid Card connected';

    function getStatus(): string
    {
        return self::STATUS_MESSAGE;
    }
}
