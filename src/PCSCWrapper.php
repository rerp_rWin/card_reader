<?php

namespace CardReader;

use CardReader\Dto\FatalMessageDto;
use CardReader\Exception\StatusFalseException;

class PCSCWrapper
{
    public $context;
    public $connection;

    function __construct()
    {
        $this->context = scard_establish_context();
    }

    function __destruct() {
        $this->releaseContext();
    }

    function connect($reader)
    {
        return $this->connection = scard_connect($this->context, $reader, 2);
    }

    function resetConnection()
    {
        $this->connection = null;
    }

    function disconnect()
    {
        if ($this->connection === null) {
            return true;
        }
        return scard_disconnect($this->connection);
    }

    function listReaders()
    {
        return scard_list_readers($this->context);
    }

    function transmit($apdu)
    {
        $return = scard_transmit($this->connection, $apdu);

        if (!$return || !str_ends_with($return, '9000')) {
            throw new \Exception("Unable to read from card error (response: $return)");
        }

        return substr($return, 0, -4);
    }

    function readBytes($apdu)
    {
        return scard_transmit($this->connection, $apdu);
    }

    function isValidContext()
    {
        return scard_is_valid_context($this->context);
    }

    function releaseContext()
    {
        return scard_release_context($this->context);
    }

    function getStatus()
    {
        if (!$this->connection) {
            throw new StatusFalseException();
        }
        return scard_status($this->connection);
    }
}
