<?php
declare(strict_types=1);

require "./vendor/autoload.php";
require "config.php";

use CardReader\WebsocketCardConnection;
use Ratchet\WebSocket\WsServer;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;

$loopThroughCardState = new \CardReader\LoopThroughCardState();
$cardConnection = new WebsocketCardConnection();

$loopThroughCardState->setCardConnection($cardConnection);
$cardConnection->setLoopThroughCardState($loopThroughCardState);

$ws = new WsServer($cardConnection);

// Make sure you're running this as root
$server = IoServer::factory(
    new HttpServer($ws),
    8081,
    '127.0.0.1'
);

$server->loop->addPeriodicTimer(0.5, $loopThroughCardState);

$server->run();
