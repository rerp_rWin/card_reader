<?php

function byteArrayToHex(array $hex){
    $chars = array_map("chr", $hex);
    $bin = join($chars);
    return bin2hex($bin);;
}


class COMMANDS {
    public const SELECT_MF = ['00', 'A4', '04', '0C', '07', 'D2', '76', '00', '01', '44', '80', '00'];
    public const SELECT_HCA = ['00', 'A4', '04', '0C', '06', 'D2', '76', '00', '00', '01', '02'];
    public const SELECT_FILE_PD = ['00', 'B0', '81', '00', '02'];
    public const SELECT_FILE_VD = ['00', 'B0', '82', '00', '08'];

    public static function READ_BINARY ($offset, $length) {
        // Split offset number into 2 bytes (big endian)
        $offsetBytes = [$offset >> 8 & 0xFF, $offset & 0xFF];
        return byteArrayToHex([0x00, 0xB0, $offsetBytes[0], $offsetBytes[1], $length]);
    }
}

$commands_AT = [
    'SELECT_MF' => ['00', 'A4', '00', '0C'],
    'SELECT_HCA' => ['00', 'A4', '04', '0C', '08', 'D0', '40', '00', '00', '17', '01', '01', '01'],
    'SELECT_FILE_PD' => ['00', 'A4', '02', '0C', '02', 'EF', '01']
];



$possibleAtrsForAustrianHealthCards = [
    "3bdf97008131fe588031b05202056405a100ac73d622c021",
    "3bdd96ff81b1fe451f038031b052020364041bb422810518",
    "3bdf18008131fe588031b05202046405c903ac73b7b1d422",
    "3bbd18008131fe45805102670414b10101020081053d",
    "3bbd18008131fe45805102670518b102020201810531",
    "3bbd18008131fe45805103670414b10101020081053c"
];
